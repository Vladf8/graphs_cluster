from matplotlib import pyplot


fig = pyplot.figure()

ax_1 = fig.add_subplot(1, 2, 1)
ax_2 = fig.add_subplot(1, 2, 2)

ax_1.set(title='128х128', xlabel='Time, sec.', ylabel='Number of images')
ax_2.set(title='1000х500', xlabel='Time, sec.', ylabel='Number of images')

x1 = [271, 542, 3000]
y1 = [5000, 10000, 50000]
x2 = [325, 745, 4800]

ax_1.plot(x1, y1, '-o', x2, y1, '--o')
ax_1.grid()
ax_1.legend(['Cluster', 'PC'])

x3 = [888, 2195, 2807]
y2 = [500, 1500, 2000]
x4 = [1021, 2634, 3649]

ax_2.plot(x3, y2, '-o', x4, y2, '--o')
ax_2.grid()
ax_2.legend(['Cluster', 'PC'])

pyplot.show()