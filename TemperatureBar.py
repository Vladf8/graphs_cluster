from matplotlib import pyplot
import os


# Temp bar
names = os.listdir('temp')
data = []
names_ticks = []
for name in names:
    file = open('./temp/' + name)
    avg = 0
    num_lines = 0
    for line in file:
        num_lines += 1
        avg += float(line.split(':')[1]) / 1000
    data.append((avg // num_lines))
    names_ticks.append(name.split('-')[1])
    print('./temp/' + name, avg // num_lines)
x = [i for i in range(1, len(data)+1)]
pyplot.bar(x, data)
pyplot.grid(True)
pyplot.title('First Come First Served')
pyplot.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], names_ticks)
pyplot.xlabel('Number of nodes')
pyplot.ylabel('Temperature, ℃')
pyplot.savefig('./result/Temp-FCFS-2k.png')
pyplot.show()